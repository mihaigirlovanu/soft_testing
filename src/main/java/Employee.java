public class Employee {

    private String name;
    private String email;
    private int varsta;
    private int salariu;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getVarsta() {
        return varsta;
    }

    public void setVarsta(int varsta) {
        this.varsta = varsta;
    }

    public int getSalariu() {
        return salariu;
    }

    public void setSalariu(int salariu) {
        if (salariu < 0) {
            throw new IllegalArgumentException("Salariul nu poate fi negativ");
        } else {
            this.salariu = salariu;
        }
    }

    public Employee(String name, String email, int varsta, int salariu) {
        this.name = name;
        this.email = email;
        this.varsta = varsta;
        this.salariu = salariu;
    }

    public Employee(){}
}
