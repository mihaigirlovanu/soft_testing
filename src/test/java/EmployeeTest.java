import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class EmployeeTest {

    @Test
    public void setSalaryShouldThrowException(){
        //given
        Employee employee = new Employee();

        try {
            employee.setSalariu(-1000);
            fail("Nu se arunca exceptia");
        }
        catch (IllegalArgumentException i) {
            assertThat(i.getMessage().equals("Salariul nu poate fi negativ"));
        }
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, -10, -100, 10})
    public void setSalaryShouldThrowException2(int salariu){
//        given
        Employee employee = new Employee();
//        when
        IllegalArgumentException illegal = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            employee.setSalariu(salariu);
        });
//        then
//        assertThat(illegal.getMessage().equals("Salariul nu poate fi negativ"));
        assertEquals("Salariul nu poate fi negativ", illegal.getMessage());
    }

    @ParameterizedTest
    @CsvSource({"1000, 200", "2000, 400"})
    public void setSalaryShouldThrowException3(int salariu, int euroSalariu){
//        given
        EmployeeUtil employeeUtil = new EmployeeUtil();
        Employee employee = new Employee();
        employee.setSalariu(salariu);
//        when
        double convertedSalary = employeeUtil.convertSalaryToEuro(employee);
//        then
        assertEquals(euroSalariu, convertedSalary);
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForIsRetired")
    public void isRetiredShouldBeSuccesfull(int inputAge, boolean expectedIsRetiredResult){
        EmployeeUtil employeeUtil = new EmployeeUtil();
        boolean acutalIsRetiredResult = employeeUtil.isRetired(inputAge);
        Assertions.assertEquals(expectedIsRetiredResult, acutalIsRetiredResult);
    }

    public static Stream<Arguments> provideArgumentsForIsRetired(){
        return Stream.of(Arguments.of(50, false), Arguments.of(65, true), Arguments.of(70, true));
    }
    @ParameterizedTest
    @ValueSource(ints = {66, 67, 68, 69, 70})
    public void isRetiredShouldReturnTrue(int age){
        //given
        EmployeeUtil employeeUtil = new EmployeeUtil();
        boolean result;
//        when
        result = employeeUtil.isRetired(age);
//        then
        assertEquals(true, result);
    }
}
